#############################################
## segmentationData caret dataset.         ##
## Find the best parameters.               ##
## Based on the blogpost by Joseph Rickert ##
#############################################
data(segmentationData)
table(segmentationData$Class)
summary(segmentationData)
str(segmentationData)
dim(segmentationData)
head (segmentationData)

#Crear un plot de frecuencias para analizar la clase a predecir (el partido de los congresistas)
ggplot(data=segmentationData, aes(x=Class)) +
  geom_bar(color="steelblue", fill="white", width=0.7)+
  theme_minimal() +
  ggtitle("Celulas bien (WS) y mal (PS) segmentadas") +
  theme(plot.title = element_text(hjust = 0.5))

trainIndex <- createDataPartition(segmentationData$Case, p=.5,list=FALSE)
trainData <- segmentationData[trainIndex,]
testData  <- segmentationData[-trainIndex,]

trainX <-trainData[,4:61] # No me interesan las primeras 3 variables (Cell ID, Case, Class)

########################

# Definir el protocolo de entrenamiento y aprender el modelo
set.seed(1234)
ctrl <- trainControl(method="repeatedcv",   # 10-fold cross validation by default
                     repeats=5) 		        # 5 repiticiones del cv

modelSvmRB <- train(x=trainX,
                  y= trainData$Class,
                  method = "svmRadial",   # Radial kernel
                  tuneLength = 5,					# 5 ensayos con diferentes valores de parametro C
                  preProc = c("center","scale"),  # Normalizar los datos
                  trControl=ctrl)
modelSvmRB
modelSvmRB$finalModel
modelSvmRB$bestTune
# El mejor tuning de C es 2, que da un accuracy de 82.83%
# El sigma del kernel se tiene constante en 0.0149

#############################
# Ahora intentamos con otra metrica ROC, que no esta por defecto soportada por caret
# por lo que nos toca especificar un summaryFunction en el trainControl. Además, ROC requiere
# conservar las probabilidades

set.seed(1234)
ctrl <- trainControl(method="repeatedcv",   # 10fold cross validation by default
                     repeats=5,		          # 5 repiticiones del cv
                     summaryFunction=twoClassSummary,	# Use AUC ROC to pick the best model
                     classProbs=TRUE)

modelSvmRB <- train(x=trainX,
                    y= trainData$Class,
                    method = "svmRadial",   # Radial kernel
                    tuneLength = 5,					# 5 ensayos con diferentes valores de parametro C
                    preProc = c("center","scale"),  # Normalizar los datos
                    metric="ROC", #Por defecto es Accuracy
                    trControl=ctrl)
modelSvmRB
modelSvmRB$finalModel
modelSvmRB$bestTune
# El mejor tuning de C es 2, que da un ROC de 88.93%
# El sigma del kernel se tiene constante en 0.0143

#############################
# Ahora intentamos con otro kernel diferente, con uno lineal

set.seed(1234)                     

#Train and Tune the SVM
modelSvmLinear <- train(x=trainX,
                        y= trainData$Class,
                        method = "svmLinear",
                        preProc = c("center","scale"),
                        metric="ROC",
                        trControl=ctrl)	
modelSvmLinear
# ROC= 86.82%

##################

#We let the default tuning process look for the best parameters
#install.packages("pROC")
#library(pROC)


#Now compare both cv resamples on both models (50 folds = 10 folds, 5 repeats each)
rValues <- resamples(list(RB=modelSvmRB, Linear=modelSvmLinear))
rValues$values
summary(rValues)
bwplot(rValues, metric="ROC", ylab =c("linear kernel", "radial kernel"))		    # boxplot
