---
title: "TITANIC"
author: "Juan Camilo Ib��ez, Esteban Aguirre"
date: "10 de octubre de 2017"
output: html_document
---

## Taller Titanic


```{r prueba_uno}
##Paso 1

library(ggplot2)
library(lattice)
library(caret)
library(e1071)
library(knitr)

##Paso 2
train <- read.csv("train.csv")
test <- read.csv("test.csv")

##Paso 3
str(train)
str(test)

#Paso 4
class(train$Sex)

#Paso 5
summary(train)
summary(test)

#Paso 6
dim(train)
dim(test)
which(is.na(test$Fare))
test[is.na(test$Fare), ]

##PREDICCI�N BASE

##Paso 7
table(train$Survived)

##Paso 8
plot(as.factor(train$Survived))

##Paso 9
ggplot(train, aes(as.factor(Survived), fill = as.factor(Survived))) + geom_bar() + labs(title = "Cantidad de sobrevivientes", x = "Sobrevivientes", y = "Personas", fill = "Muere o vive")

##Paso 10
prop.table(table(train$Survived))
```

## Predicci�n que todos mueren


```{r prueba_dos}
##Paso 11
train$Prediction <- 0
confusionMatrix(train$Prediction, train$Survived)

##Paso 12 para enviar el archivo a Kaggle.com
test$Survived <- rep(0, 418)
resultados <- data.frame(PassengerId = test$PassengerId, Survived = test$Survived) 
write.csv(resultados, file = "todosMueren.csv", row.names = FALSE)
```

## Mujeres y ni�os primero

```{r prueba_tres}
ggplot(train, aes(Sex, fill = Survived)) + geom_bar() + labs(title = "Cantidad de sobrevivientes por sexo", x = "Sexo", y = "Personas")
ggplot(train, aes(as.factor(Survived), fill = as.factor(Survived))) + geom_bar() + facet_grid(. ~ Sex) + labs(title = "Cantidad de sobrevivientes", x = "Sobrevivientes por sexo", y = "Personas", fill = "Muere o vive")
summary(train$Sex)

##Paso 13
prop.table(table(train$Sex, train$Survived))

##Paso 14
prop.table(table(train$Sex, train$Survived), 1)
counts <- table(train$Survived, train$Sex)

##Paso 15
counts[2]/(counts[1] + counts[2])
counts[4]/(counts[3] + counts[4])

##Paso 16
train$Prediction <- 0
train$Prediction[train$Sex == "female"] <- 1
confusionMatrix(train$Prediction, train$Survived)

#Paso 17
test$Survived <- 0
test$Survived[test$Sex == "female"] <- 1
resultados <- data.frame(PassengerId = test$PassengerId, Survived = test$Survived)
write.csv(resultados, file = "mujeres.csv", row.names = FALSE)
```

## Ni�os

```{r prueba_cuatro}
##Paso 18
head(train[is.na(train$Age), ])

##Paso 19
nrow(train[is.na(train$Age), ])

##Paso 20
which(is.na(train$Age))

##Paso 21
ggplot(train, aes(Age)) + geom_histogram(bins = 20) + labs(title = "Edad de pasajeros", x = "Edad", y = "Personas")

##Paso 22
ggplot(train, aes(Age, fill = Sex)) + geom_histogram(bins = 20) + facet_grid(. ~ Sex) + labs(title = "Edad de sobrevivientes", x = "Edad", y = "Personas",fill = "Sexo")
summary(train$Age)

##Paso 23
ggplot(train, aes(Age)) + geom_density() + geom_vline(xintercept = 18, color = "red") + labs(title = "Distribucion de pasajeros por edad", x = "Edad", y = "Densidad")

##Paso 24
train$Child <- 0
train$Child[train$Age < 18] <- 1
train$Prediction <- 0
train$Prediction[train$Child == 1] <- 1
confusionMatrix(train$Prediction, train$Survived)
```

## Distribuci�n por sexo y edad

```{r prueba_cinco}
##Paso 25
aggregate(Survived ~ Child + Sex, data = train, FUN = sum)

##Paso 25
ggplot(train, aes(as.factor(Child), fill = as.factor(Survived))) + geom_bar() + facet_grid(Survived ~ Sex) + labs(title = "Comparacion de superviviencia entre ninos y adultos por sexo", x = "Nino o adulto", y = "Personas", fill = "Muere o vive")

##Paso 26
aggregate(Survived ~ Child + Sex, data = train, FUN = function(x) { sum(x)/length(x) })

##PRECIO Y CLASE DEL PASAJE
##Paso 27
summary(train$Fare)

##Paso 28
ggplot(train, aes(Fare)) + geom_histogram(binwidth = 10) + geom_vline(xintercept = 15, color = "red") + labs(title = "Distribucion de cantidades pagadas por pasajero",x = "Valor", y = "Cantidad de pagos")

##Paso 29
train$Fare2 <- "30+"
train$Fare2[train$Fare < 30 & train$Fare >= 20] <- "20-30"
train$Fare2[train$Fare < 20 & train$Fare >= 10] <- "10-20"
train$Fare2[train$Fare < 10] <- "10 o menos"
ggplot(train, aes(Fare2)) + geom_bar() + labs(title = "Distribucion ajustada de cantidades pagadas", x = "Valor", y = "Cantidad de Pagos", fill = "Muere o vive")
table(train$Fare2)

##Paso 30 yupiiiii
Pclass_survival <- table(train$Survived, train$Pclass)
ggplot(train, aes(Pclass)) + geom_bar(aes(fill = as.factor(Survived)), position = "dodge") + labs(title = "Superviviencia por clase", x = "Clase", y = "Personas", fill = "Muere o vive")
Pclass_survival[2]/(Pclass_survival[1] + Pclass_survival[2])
Pclass_survival[4]/(Pclass_survival[3] + Pclass_survival[4])
Pclass_survival[6]/(Pclass_survival[5] + Pclass_survival[6])
aggregate(Survived ~ Fare2 + Pclass + Sex, data = train, FUN = function(x) { sum(x)/length(x)})

##Paso 31
train$Prediction <- 0
train$Prediction[train$Sex == "female"] <- 1
train$Prediction[train$Sex == "female" & train$Pclass == 3 & train$Fare >= 20] <- 0
confusionMatrix(train$Prediction, train$Survived)
```
## Tarea

```{r prueba_seis}
summary(train$Age)
aggregate(Survived ~ Age + Sex, data = train, FUN = sum)

train$Prediction <- 0
train$Prediction[train$Sex == "female"] <- 1
train$Prediction[train$Sex == "female" & train$Age == 25 ] <-1
train$Prediction[train$Sex == "female" & train$Age == 21 ] <-1
train$Prediction[train$Sex == "male" & train$Age >= 60 ] <-0
confusionMatrix(train$Prediction, train$Survived)

test$Survived <- 0
test$Survived[test$Sex == "female"] <- 1
test$Survived[test$Sex == "female" & test$Pclass == 2 & test$Fare >= 20]<- 0
result <- data.frame(PassengerId = test$PassengerId, Survived =
                       test$Survived)
write.csv(result, file = "tareaPorSexoyEdad.csv", row.names = FALSE)
aggregate(Survived ~ Parch + Sex, data = train, FUN = sum)

##COLOMBIA AL MUNDIAAAAAAL
##SI SI COLOMBIA SI SI TATARETO

```